import tkinter
from gulka import Gulicka
canvas = tkinter.Canvas(width=1000, height=1000)
canvas.pack()

g = Gulicka(100,100,80,'Miško', 'yellow', canvas)
g.kresli()

while True:
    g.posun()
    canvas.after(50)

canvas.mainloop()