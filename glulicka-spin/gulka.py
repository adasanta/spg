class Gulicka:
    def __init__(self, x, y, priemer, meno, farba, canvas):
        self.x=x
        self.y=y
        self.priemer = priemer
        self.meno = meno
        self.farba = farba
        self.canvas = canvas
        self.uhol = 0

    def kresli(self):
        r = self.priemer / 2
        self.canvas.create_oval(self.x-r, self.y-r, self.x+r, self.y+r, fill=self.farba, tag=self.meno)
        self.canvas.create_text(self.x, self.y, text=self.meno, font='Arial 14 bold', angle=self.uhol, tag=self.meno)
        self.canvas.update();

    def posun(self):
        self.canvas.delete(self.meno)
        self.x += (self.priemer * 3.14)/12
        self.uhol -= 30
        self.kresli()
        self.canvas.update()