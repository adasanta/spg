import tkinter
canvas=tkinter.Canvas(width=800, height=600, bg='white')
canvas.pack()

class Gulicka:
    def __init__(self, x, y, r, meno, farba):
        self.x=x
        self.y=y
        self.r=r
        self.meno=meno
        self.farba=farba
        self.kresli()
    def kresli(self):
        canvas.delete(self.meno)
        canvas.create_oval(self.x-self.r, self.y-self.r, self.x+self.r, self.y+self.r, fill=self.farba, tag=self.meno)
        canvas.create_text(self.x, self.y, text=self.meno, font='Arial 12')
    def posun(self, xx, yy):
        self.x=xx
        self.y=yy
        self.kresli()

g1 = Gulicka(100,100,30,'Misko','orange')
g2 = Gulicka(200,200,50,'Vierka','blue')
        
        
