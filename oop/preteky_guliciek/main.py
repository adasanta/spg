import tkinter
import gulicka
import random

canvas=tkinter.Canvas(width=1200, height=800, bg='white')
canvas.pack()

namePool = ["Viktor", "Dávid", "Paťo", "Dominik", "Adam", "Radovan", "Bohumil", "Pankrác", "Bonifác", "Hortenzia", "Frederik", "Scarlet", "Vladimir", "Ahmed", "Veronika", "Fatima", "Darja", "Afrodita"]
colorsPool = ["skyblue", "pink", "orange", "grey"]

gulicky = []
for i in range(6):
    gulicky.append(gulicka.Gulicka(100,50+i*100,35,namePool[i],random.choice(colorsPool), 0, canvas))

winner = False
winnerName = ''
while winner == False:
    for i in range(6):
        newx = gulicky[i].mojex() + random.randrange(0, 20)
        gulicky[i].posun(newx)
        if newx > 1100:
            winner = True
            gulicky[i].skonci(canvas)

    canvas.update()
    canvas.after(100)

def hore(event):
    global gulicky
    gulicky[0].posun(gulicky[0].x + random.randrange(0, 20), gulicky[0].y)
def dole(event):
    global gulicky
    gulicky[0].posun(gulicky[0].x - random.randrange(0, 20), gulicky[0].y)

canvas.bind_all('<Up>', hore)
canvas.bind_all('<Down>', dole)

canvas.mainloop()