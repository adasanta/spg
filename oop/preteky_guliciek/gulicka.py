import math
import random
class Gulicka:
    def __init__(self, x, y, r, meno, farba, uhol, canvas):
        self.__x=x
        self.__y=y
        self.__r=r
        self.__meno=meno
        self.__farba=farba
        self.__uhol = uhol
        self.__canvas=canvas
        self.kresli()

    def kresli(self):
        self.__canvas.delete(self.__meno)
        self.__canvas.create_oval(self.__x-self.__r, self.__y-self.__r, self.__x+self.__r, self.__y+self.__r, fill=self.__farba, tag=self.__meno)
        self.__canvas.create_text(self.__x, self.__y, text=self.__meno, font='Arial 12', tag=self.__meno, angle=self.__uhol)

    def posun(self, __x):
        posun = __x - self.__x
        self.__x=__x
        self.__uhol = self.__uhol - math.degrees(math.atan(posun/self.__r))
        self.kresli()

    def mojex(self):
        return self.__x

    def skonci(self, __canvas):
        __canvas.create_text(600, 600, text='Vyhral hrac '+self.__meno, font='Arial 12', fill='red')