import tkinter
import random
canvas=tkinter.Canvas(width=800, height=600, bg='white')
canvas.pack()

class Kvet:
    def __init__(self, x, yk, yz, r, f, id):
        self.x = x
        self.yk = yk
        self.yz = yz
        self.f = f
        self.id = id
        self.r = r
        canvas.create_rectangle(self.x-2, int(canvas["width"]), self.x + 2, self.yk, fill="green", tag='a'+self.id)
        canvas.create_oval(self.x - 2*self.r, self.yk - 2*self.r, self.x + 2*self.r, self.yk + 2*self.r, fill=self.f, tag='a'+self.id)
        canvas.create_oval(self.x - self.r, self.yk - self.r, self.x + self.r, self.yk + self.r, fill="yellow", tag='a'+self.id)
    def posun(self):
        self.yk = self.yk - 10
        canvas.delete('a'+self.id)
        canvas.create_rectangle(self.x-2, int(canvas["width"]), self.x + 2, self.yk, fill="green", tag='a'+self.id)
        canvas.create_oval(self.x - 2*self.r, self.yk - 2*self.r, self.x + 2*self.r, self.yk + 2*self.r, fill=self.f, tag='a'+self.id)
        canvas.create_oval(self.x - self.r, self.yk - self.r, self.x + self.r, self.yk + self.r, fill="yellow", tag='a'+self.id)

def vytvor(event):
    global index
    index = index + 1
    color = random.choice(["red", "pink", "orange", "skyblue"])
    k.append(Kvet(event.x, event.y, int(canvas["width"]), 10, color, 'a'+str(index)))

index = 0
canvas.bind('<Button-1>', vytvor)

k = []
    
while True:
    for i in k:
        if int(i.yk) > 40:
            i.posun()
    canvas.update()
    canvas.after(1000)
