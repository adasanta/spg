import robot
import tkinter

canvas = tkinter.Canvas(height = 1000, width = 1000)
pero = robot.Robot(canvas)

def userInput(event):
    print(event.char)
    char = event.char
    if char == "w":
        pero.pohni(0, -1)
    if char == "a":
        pero.pohni(-1, 0)
    if char == "s":
        pero.pohni(0, 1)
    if char == "d":
        pero.pohni(1, 0)
    if char == "z":
        pero.zmenPero()
    if char == "d":
        canvas.delete("all")
        pero.obnov()

canvas.bind_all("<Key>", userInput)
canvas.pack()
canvas.mainloop()

