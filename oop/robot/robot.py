class Robot:
    r = 10
    tag = "pen"
    color = "black"
    krok = 50

    def __init__(self, canvas):
        self.__x = 100
        self.__y = 100
        self.__pero = True
        self.__canvas = canvas
        self.__kresli()

    def __kresli(self):
        self.__canvas.delete(Robot.tag)
        self.__canvas.create_oval(self.__x - Robot.r, self.__y - Robot.r, self.__x + Robot.r, self.__y + Robot.r, tag = Robot.tag, fill = Robot.color)

    def zmenPero(self):
        self.__pero = not self.__pero

    def obnov(self):
        self.__x = 100
        self.__y = 100
        self.__kresli()

    def pohni(self, deltaX, deltaY):
        predicX = self.__x + (deltaX * Robot.krok)
        predicY = self.__y + (deltaY * Robot.krok)
        if self.__pero:
            self.__canvas.create_line(self.__x, self.__y, predicX, predicY)
        self.__x = predicX
        self.__y = predicY
        self.__kresli()