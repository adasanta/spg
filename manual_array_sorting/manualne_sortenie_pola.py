import tkinter
from tkinter import *
import random

c = tkinter.Canvas(width='1000', height='600', bg='white')
c.pack()

pole = list()
oblacik = -1
a = 0
check=True

def generuj(pocet):
    c.delete('all')
    global pole
    for i in range(0, pocet):
        pole.append(random.randint(1, 100))
        c.create_rectangle((i + 1) * 40, 40, (i + 2) * 40, 80)
        c.create_text((i + 1) * 40 + 20, 60, text=pole[i])
    skontroluj()

def oznac(x, y):
    global oblacik
    global pole
    global a
    c.delete('all')
    for i in range(0, len(pole)):
        c.create_rectangle((i + 1) * 40, 40, (i + 2) * 40, 80)
        c.create_text((i + 1) * 40 + 20, 60, text=pole[i])
    oblacik = (x // 40) - 1
    a = (x // 40)
    if (y > 40 and y <= 80):
        c.create_rectangle((a) * 40, 40, (a + 1) * 40, 80, outline='red', width=3)

def vymen(x, y):
    global pole
    global oblacik
    c.delete('all')
    a = (x // 40) - 1
    if a != oblacik:
        pole[a] += pole[oblacik]
        pole[oblacik] = pole[a] - pole[oblacik]
        pole[a] -= pole[oblacik]
    for i in range(0, len(pole)):
        c.create_rectangle((i + 1) * 40, 40, (i + 2) * 40, 80)
        c.create_text((i + 1) * 40 + 20, 60, text=pole[i])
    oblacik = -1
    skontroluj()

def skontroluj():
    global check
    check = True
    for i in range(len(pole)-1):
        if pole[i] > pole[i+1]:
            check = False
            break
    c.create_text(200, 200, text=str(check))
            
def hlavna(event):
    global oblacik
    if oblacik == -1:
        oznac(event.x, event.y)
    else:
        vymen(event.x, event.y)

c.bind('<Button-1>', hlavna)

generuj(12)
